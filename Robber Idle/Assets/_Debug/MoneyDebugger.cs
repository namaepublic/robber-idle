﻿#if UNITY_EDITOR
using MightyAttributes;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "MoneyDebugger", menuName = "Debugger/Money Debugger")]
public class MoneyDebugger : ScriptableObject
{
    private MoneyManager GetMoneyManager() => ReferencesUtilities.FindFirstObject<MoneyManager>();

    [BeginHorizontal] public ulong amount;

    [Button]
    private void AddAmount() => GetMoneyManager()?.AddAmount(amount);

    [Button]
    [EndHorizontal]
    private void RemoveAmount() => GetMoneyManager()?.TryRemoveAmount(amount);

    [Button]
    private void ResetAmount() => GetMoneyManager().ResetAmount();
    
    [OnInspectorGUI]
    private void OnInspectorGUI() => EditorGUILayout.LabelField("Money Amount", MoneyManager.DisplayAmount());
}
#endif