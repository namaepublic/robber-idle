﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "SlowProgressCurve", menuName = "Progress Curve/Slow Progress Curve")]
public class SlowProgressCurve : BaseProgressCurve
{
    public override double Evaluate(double x)
    {
        if (x < _offset.x) return 0;
        return (ushort) (Math.Pow(x - _offset.x, 1 / _curveForce) * _multiplier + _offset.y);
    }

    public override double Revert(double x)
    {
        if (x < _offset.y) return 0;
        return (ulong) (Math.Pow((x - _offset.y) / _multiplier, _curveForce) + _offset.x);
    }

#if UNITY_EDITOR
    protected override string GetFormula() => $"(x - {_offset.x})^(1 / {_curveForce}) {_multiplier} + {_offset.y}";
#endif
}