﻿using MightyAttributes;
#if UNITY_EDITOR
using MightyAttributes.Editor;
using UnityEditor;
#endif
using UnityEngine;

public abstract class BaseProgressCurve : ScriptableObject
{
    [SerializeField] protected Vector2 _offset;
    public float _curveForce;
    public float _multiplier;

    public Vector2 Offset => _offset;
    public float CurveForce => _curveForce;
    public float Multiplier => _multiplier;

    public abstract double Evaluate(double x);
    public abstract double Revert(double x);

#if UNITY_EDITOR
    private ulong m_x;

    protected abstract string GetFormula();

    [Title("Debug")]
    [OnInspectorGUI]
    private void DebugCurve()
    {
        EditorGUILayout.LabelField(GetFormula());

        MightyGUIUtilities.DrawLine(Color.grey);

        m_x = (ulong) EditorGUILayout.LongField("X", (long) m_x);

        EditorGUILayout.LabelField("Result", Evaluate(m_x).ToString());
    }
#endif
}