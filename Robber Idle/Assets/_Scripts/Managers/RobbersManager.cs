﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobbersManager : BaseManager
{
    [SerializeField] private RobberBehaviour _robberPrefab;
    private readonly List<RobberBehaviour> m_robbers = new List<RobberBehaviour>();

    public override void Init()
    {
    }

    public void SpawnRobbers()
    {
        for (var i = 0; i < (ushort) InstanceManager.UpgradesManager.GetValue(UpgradeType.Unit); i++)
            SpawnRobber();
    }

    public RobberBehaviour SpawnRobber()
    {
        var robber = Instantiate(_robberPrefab, InstanceManager.BiomeManager.RobbersTransform);

        robber.Init();
        robber.ForcePosition(InstanceManager.LevelManager.SellingArea.Position);
        robber.SetStrength((ushort) InstanceManager.UpgradesManager.GetValue(UpgradeType.Strength));

        m_robbers.Add(robber);
        return robber;
    }

    public void ReleaseRobbers()
    {
        if (m_robbers.Count == 0) SpawnRobbers();
        StartCoroutine(ReleaseRobbersCoroutine());
    }

    private IEnumerator ReleaseRobbersCoroutine()
    {
        for (var i = 0; i < m_robbers.Count; i++)
        {
            yield return TimeManager.ReleaseTime;
            m_robbers[i].SelectNextTarget();
        }
    }

    public void SetStrength(ushort strength)
    {
        for (var i = 0; i < m_robbers.Count; i++)
            m_robbers[i].SetStrength(strength);
    }

    public void UpdateManager()
    {
        for (var i = 0; i < m_robbers.Count; i++)
            m_robbers[i].UpdateBehaviour();
    }
}