﻿#if UNITY_EDITOR
using System.Linq;
#endif
using MightyAttributes;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GUIManager : BaseManager
{
    [SerializeField] private TextMeshProUGUI _levelText;
    [SerializeField] private Image _xpBar;
    [SerializeField] private TextMeshProUGUI _moneyText;
    [SerializeField] private TextMeshProUGUI _unitText;
    [SerializeField, FindObjects] private UpgradeButtonBehaviour[] _upgradeButtons;

    private UpgradesManager m_upgradesManager;

    [Button]
    public override void Init()
    {
        m_upgradesManager = InstanceManager.UpgradesManager;

        RefreshLevelPanel();
        RefreshMoneyText();
        foreach (var upgradeButton in _upgradeButtons)
            upgradeButton.Init();
    }

    public void RefreshBiomeDependantGUI()
    {
        RefreshUnitText();
        foreach (var upgradeButton in _upgradeButtons)
        {
            upgradeButton.UpdateLevel();
            upgradeButton.SetEnable(m_upgradesManager.CanUpgrade(upgradeButton.Type) &&
                                    MoneyManager.CanSpend(m_upgradesManager.GetCost(upgradeButton.Type)));
        }
    }

    public void RefreshLevelPanel()
    {
        _levelText.text = (InstanceManager.PlayerLevelManager.GetPlayerLevel(out var progression) + 1).ToString();
        _xpBar.fillAmount = progression;
    }

    public void RefreshMoneyText() => _moneyText.text = MoneyManager.DisplayAmount();

    public void RefreshUnitText() => _unitText.text = ((int) m_upgradesManager.GetValue(UpgradeType.Unit)).ToString();

    public void RefreshUpgradeButtons()
    {
        foreach (var upgradeButton in _upgradeButtons)
            upgradeButton.SetEnable(m_upgradesManager.CanUpgrade(upgradeButton.Type) &&
                                    MoneyManager.CanSpend(m_upgradesManager.GetCost(upgradeButton.Type)));
    }

    public void RefreshUpgradeButton(UpgradeType type) =>
        _upgradeButtons[(byte) type].SetEnable(m_upgradesManager.CanUpgrade(type) &&
                                               MoneyManager.CanSpend(m_upgradesManager.GetCost(type)));

#if UNITY_EDITOR
    [OnInspectorGUI(false)]
    private void OnInspectorGUI()
    {
        _upgradeButtons = _upgradeButtons.OrderBy(ub => (byte) ub.Type).ToArray();
    }
#endif
}