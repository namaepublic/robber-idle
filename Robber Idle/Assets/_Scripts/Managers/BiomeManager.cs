﻿using MightyAttributes;
using UnityEngine;

public class BiomeManager : BaseManager
{
    [SerializeField] private BiomeModel _biomeModel;
    [SerializeField, FindObjectWithTag("Robbers"), ReadOnly] private Transform _robbersTransform;

    [SerializeField, FindObjects, ReadOnly] private LevelManager[] _levels;

    public byte BiomeIndex => (byte) (_biomeModel.SceneIndex - 1);
    public Transform RobbersTransform => _robbersTransform;
    
    private void Awake() => Init();

    public override void Init()
    {
        InstanceManager.BiomeManager = this;

        LoadLevel(SavedDataServices.LevelIndex);
        
        InstanceManager.GUIManager.RefreshBiomeDependantGUI();
        InstanceManager.GameManager.Play();
    }

    public void NextLevel()
    {
        Debug.Log("Next Level");
        var index = ++SavedDataServices.LevelIndex;
        if (index >= _levels.Length) index = SavedDataServices.LevelIndex = 0;
        
        LoadLevel(index);
    }

    public void LoadLevel(int index)
    {
        for (var i = 0; i < _levels.Length; i++)
        {
            if (i == index) _levels[i].Init();
            else _levels[i].ResetLevel();
        }
        
        InstanceManager.RobbersManager.ReleaseRobbers();
    }
}
