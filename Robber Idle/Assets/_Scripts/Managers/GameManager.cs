﻿using EncryptionTool;
using MightyAttributes;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : BaseManager
{
    [SerializeField] private Transform _cameraTransform;
    [SerializeField, FindObject, ReadOnly] private InstanceManager _instanceManager;
    [SerializeField, FindObject, ReadOnly] private EncryptionInitializer _encryptionInitializer;

    [SerializeField, FindAssets, Reorderable(false, options: ArrayOption.DontFold)]
    private BiomeModel[] _biomeModels;

    private bool m_play;
    private RobbersManager m_robbersManager;
    private LevelManager m_levelManager;

    public Quaternion CameraRotation { get; private set; }
    
    private void Awake() => Init();

    public override void Init()
    {
        SavedDataServices.Init(_encryptionInitializer);
        SavedDataServices.LoadEverythingFromLocal();
        
        _instanceManager.Init();

        m_robbersManager = InstanceManager.RobbersManager;
        CameraRotation = _cameraTransform.rotation;
        
        Pause();

        LoadBiome(SavedDataServices.BiomeIndex);
    }

    public void Play()
    {
        m_play = true;
        m_levelManager = InstanceManager.LevelManager;
        InstanceManager.UpgradesManager.Init();
        TimeManager.SetTimeScale(InstanceManager.UpgradesManager.GetValue(UpgradeType.Speed));
    }

    public void Pause() => m_play = false;

    public void NextBiome()
    {
        SavedDataServices.LevelIndex = 0;
        UnloadBiome(SavedDataServices.BiomeIndex);
        LoadBiome(++SavedDataServices.BiomeIndex);
    }

    public void LoadBiome(int index) => SceneManager.LoadScene(_biomeModels[index].SceneIndex, LoadSceneMode.Additive);
    public void UnloadBiome(int index) => SceneManager.UnloadSceneAsync(_biomeModels[index].SceneIndex);

    private void Update()
    {
        if (!m_play) return;
        m_robbersManager.UpdateManager();
        m_levelManager.UpdateManager();
    }
}