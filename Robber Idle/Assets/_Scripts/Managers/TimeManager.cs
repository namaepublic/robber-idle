﻿using UnityEngine;

public class TimeManager : BaseManager
{
    public static readonly WaitForSeconds SellTime = new WaitForSeconds(.2f);
    public static readonly WaitForSeconds ReleaseTime = new WaitForSeconds(.3f);
    private static float m_fixedDeltaTime;

    public override void Init()
    {
        m_fixedDeltaTime = Time.fixedDeltaTime;
        SetTimeScale(1);
    }

    public static void SetTimeScale(float timeScale)
    {
        Time.timeScale = timeScale;
        Time.fixedDeltaTime = m_fixedDeltaTime * timeScale;
    }
}
