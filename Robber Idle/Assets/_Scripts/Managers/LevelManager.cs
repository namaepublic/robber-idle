﻿using MightyAttributes;
using UnityEngine;

public class LevelManager : BaseManager
{
    [SerializeField, GetComponentInChildren, ReadOnly]
    private SellingAreaBehaviour _sellingArea;

    [SerializeField, GetComponentsInChildren, ReadOnly]
    private CollectibleBehaviour[] _collectibles;

    public SellingAreaBehaviour SellingArea => _sellingArea;
    public CollectibleBehaviour[] Collectibles => _collectibles;

    public override void Init()
    {
        InstanceManager.LevelManager = this;

        _sellingArea.Init();
        foreach (var collectible in _collectibles)
            collectible.Init();

        gameObject.SetActive(true);
    }

    public void UpdateManager()
    {
        foreach (var collectible in _collectibles) collectible.UpdateBehaviour();
    }

    public void ResetLevel() => gameObject.SetActive(false);

    public bool HasAvailableCollectibles()
    {
        foreach (var collectible in _collectibles)
            if (collectible.State <= CollectibleState.Targeted &&
                collectible.Weight <= InstanceManager.UpgradesManager.GetValue(UpgradeType.Strength))
                return true;

        return false;
    }
    
    public bool IsCompleted()
    {
        foreach (var collectible in _collectibles)
            if (collectible.State != CollectibleState.Sold &&
                collectible.Weight <= InstanceManager.UpgradesManager.GetValue(UpgradeType.Strength))
                return false;

        return true;
    }
}