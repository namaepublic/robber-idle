﻿using MightyAttributes;
using UnityEditor;
using UnityEngine;

public class InstanceManager : BaseManager
{
    // @formatter:off
    [Title("Instances")] 
    [SerializeField, FindObject, ReadOnly] private GameManager _gameManager;
    [SerializeField, FindObject, ReadOnly] private GUIManager _guiManager;
    [SerializeField, FindObject, ReadOnly] private TimeManager _timeManager;
    [SerializeField, FindObject, ReadOnly] private MoneyManager _moneyManager;
    [SerializeField, FindObject, ReadOnly] private PlayerLevelManager _playerLevelManager;
    [SerializeField, FindObject, ReadOnly] private UpgradesManager _upgradesManager;
    [SerializeField, FindObject, ReadOnly] private RobbersManager _robbersManager;
    [SerializeField, FindObject, ReadOnly] private SellingManager _sellingManager;
    // @formatter:on
    
    private BiomeManager m_biomeManager;
    private LevelManager m_levelManager;

    public static GameManager GameManager => Instance._gameManager;
    public static GUIManager GUIManager => Instance._guiManager;
    public static TimeManager TimeManager => Instance._timeManager;
    public static MoneyManager MoneyManager => Instance._moneyManager;
    public static PlayerLevelManager PlayerLevelManager => Instance._playerLevelManager;
    public static UpgradesManager UpgradesManager => Instance._upgradesManager;
    public static SellingManager SellingManager => Instance._sellingManager;
    public static RobbersManager RobbersManager => Instance._robbersManager;

    public static BiomeManager BiomeManager
    {
        get
        {
#if UNITY_EDITOR
            if (!EditorApplication.isPlaying && Instance.m_biomeManager == null)
                Instance.m_biomeManager = ReferencesUtilities.FindFirstObject<BiomeManager>();
#endif
            return Instance.m_biomeManager;
        }
        set => Instance.m_biomeManager = value;
    }

    public static LevelManager LevelManager
    {
        get
        {
#if UNITY_EDITOR
            if (!EditorApplication.isPlaying && Instance.m_levelManager == null)
                Instance.m_levelManager = ReferencesUtilities.FindFirstObject<LevelManager>();
#endif
            return Instance.m_levelManager;
        }
        set => Instance.m_levelManager = value;
    }

    private static InstanceManager m_instance;

    private static InstanceManager Instance
    {
        get
        {
#if UNITY_EDITOR
            if (!EditorApplication.isPlaying && m_instance == null)
                m_instance = ReferencesUtilities.FindFirstObject<InstanceManager>();
#endif
            return m_instance;
        }
    }

    public override void Init()
    {
        m_instance = this;
        
        _guiManager.Init();
        _timeManager.Init();
        _moneyManager.Init();
        _playerLevelManager.Init();
        _upgradesManager.Init();
        _sellingManager.Init();
        _robbersManager.Init();
    }
}