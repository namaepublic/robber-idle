﻿using UnityEngine;
using UnityEngine.Events;

public class PlayerLevelManager : BaseManager
{
    [SerializeField] private SlowProgressCurve _playerLevelCurve;
    [SerializeField] private UnityEvent _onStealCountChange;

    public override void Init()
    {
    }

    public void IncreaseStealCount()
    {
        SavedDataServices.StealCount++;
        _onStealCountChange.Invoke();
    }

    public ushort GetPlayerLevel() => (ushort) _playerLevelCurve.Evaluate(SavedDataServices.StealCount);

    public ushort GetPlayerLevel(out float progression)
    {
        var stealCount = SavedDataServices.StealCount;
        var level = (ushort) _playerLevelCurve.Evaluate(stealCount);
        var previousLevelNeeded = level > 0 ? _playerLevelCurve.Revert(level) : 0;

        progression = (float) (stealCount - previousLevelNeeded) / (float) (_playerLevelCurve.Revert(level + 1) - previousLevelNeeded);

        return level;
    }
}