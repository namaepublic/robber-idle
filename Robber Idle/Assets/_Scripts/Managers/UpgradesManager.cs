﻿using System;
using System.Numerics;
using MightyAttributes;
#if UNITY_EDITOR
using System.Linq;
using MightyAttributes.Editor;
using UnityEditor;
#endif
using UnityEngine;

public class UpgradesManager : BaseManager
{
    [SerializeField, FindAssets] private Upgrade[] _upgrades;

    public override void Init()
    {
    }

    public Upgrade GetUpgrade(UpgradeType type) => _upgrades[(byte) type];

    public ushort GetLevel(UpgradeType type)
    {
        if (SavedDataServices.TryGetUpgradeLevel(type, InstanceManager.BiomeManager.BiomeIndex, out var level)) 
            return level;
        SavedDataServices.ResetUpgradeLevel(type, InstanceManager.BiomeManager.BiomeIndex);
        return 0;
    }

    public bool CanUpgrade(UpgradeType type) => _upgrades[(byte) type].CanUpgrade(InstanceManager.BiomeManager.BiomeIndex, GetLevel(type));

    public BigInteger GetCost(UpgradeType type) => _upgrades[(byte) type].GetCost(InstanceManager.BiomeManager.BiomeIndex, GetLevel(type));

    public float GetValue(UpgradeType type) => _upgrades[(byte) type].GetValue(InstanceManager.BiomeManager.BiomeIndex, GetLevel(type));

    public bool TryToBuy(UpgradeType type)
    {
        if (!InstanceManager.MoneyManager.TryRemoveAmount(GetCost(type))) return false;
        
        SavedDataServices.IncreaseUpgradeLevel(type, InstanceManager.BiomeManager.BiomeIndex);
        InstanceManager.GUIManager.RefreshUpgradeButton(type);

        switch (type)
        {
            case UpgradeType.Speed:
                TimeManager.SetTimeScale(GetValue(type));
                break;
            case UpgradeType.Strength:
                InstanceManager.RobbersManager.SetStrength((ushort) GetValue(type));
                break;
            case UpgradeType.Unit:
                InstanceManager.RobbersManager.SpawnRobber().SelectNextTarget();
                InstanceManager.GUIManager.RefreshUnitText();
                break;
        }
        return true;
    }
    
#if UNITY_EDITOR
    [OnInspectorGUI]
    private void OnInspectorGUI()
    {
        _upgrades = _upgrades?.OrderBy(x => (byte) x.Type).ToArray();

        if (InstanceManager.BiomeManager == null) return;
        
        var names = Enum.GetNames(typeof(UpgradeType));
        var length = names.Length;

        MightyGUIUtilities.DrawLine(Color.grey);

        for (var i = 0; i < length; i++)
            EditorGUILayout.LabelField(names[i], $"level: {GetLevel((UpgradeType) i)} value: {GetValue((UpgradeType) i)}");
    }
#endif
}