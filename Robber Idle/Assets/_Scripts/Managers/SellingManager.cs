﻿using System.Numerics;
using UnityEngine;

public class SellingManager : BaseManager
{
    [SerializeField] private FastProgressCurve _collectiblePriceCurve;

    public override void Init()
    {
    }

    private BigInteger GetPrice(CollectibleBehaviour collectible) =>
        new BigInteger(_collectiblePriceCurve.Evaluate(InstanceManager.PlayerLevelManager.GetPlayerLevel()) * collectible.BasePrice);

    public void Sell(CollectibleBehaviour collectible)
    {
        var price = GetPrice(collectible);
        InstanceManager.MoneyManager.AddAmount(price);
        InstanceManager.PlayerLevelManager.IncreaseStealCount();

        collectible.OnCollectibleSold(price);
    }
}