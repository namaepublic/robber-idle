﻿using MightyAttributes;
using UnityEngine;

[CreateAssetMenu(menuName = "Model/Biome Model", fileName = "BiomeModel")]
public class BiomeModel : ScriptableObject
{
    [SerializeField, SceneDropdown] private byte _sceneIndex;

    public byte SceneIndex => _sceneIndex;
}