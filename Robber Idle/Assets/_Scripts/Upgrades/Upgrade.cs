﻿using System;
using System.Linq;
using System.Numerics;
using MightyAttributes;
using UnityEngine;

public enum UpgradeType : byte
{
    Speed,
    Strength,
    Unit,
}

[Line(ArrayDecoratorPosition.BeforeElements)]
[ItemNames("GetBiomeNames", true, ArrayOption.HideSizeField | ArrayOption.DontFold)]
[Nest(NestOption.DontFold | NestOption.BoldLabel | NestOption.DontIndent)]
public class UpgradesAttribute : BaseWrapperAttribute
{
#if UNITY_EDITOR
    private string[] GetBiomeNames() => typeof(BiomeModel).FindAssetsOfType().Select(a => a.name).ToArray();
#endif
}

[Serializable]
public class UpgradeCurve
{
    [SerializeField, MinValue(0)] private ushort _maxLevel;

    [SerializeField, MinValue(0)] private ulong _baseCost;
    [SerializeField] private AnimationCurve _costCurve;

    [SerializeField] private float _defaultValue;
    [SerializeField] private AnimationCurve _valueCurve;

    public ushort MaxLevel => _maxLevel;
    public BigInteger GetCost(ushort level) => (BigInteger) (_baseCost + _baseCost * _costCurve.Evaluate((float) level / _maxLevel));
    public float GetValue(ushort level) => _defaultValue + _valueCurve.Evaluate((float) level / _maxLevel);
}

[CreateAssetMenu(fileName = "Upgrade", menuName = "Upgrade")]
public class Upgrade : ScriptableObject
{
    [Title("GFX")] [SerializeField] private string _name;
    [SerializeField] private Sprite _icon;
    [SerializeField] private Color _color;

    [Title("Upgrade")] [SerializeField] private UpgradeType _type;

    [SerializeField, Upgrades] private UpgradeCurve[] _upgradeCurves;

    public string Name => _name;
    public Sprite Icon => _icon;
    public Color Color => _color;

    public UpgradeType Type => _type;

    public bool CanUpgrade(byte biomeIndex, ushort level) => level < _upgradeCurves[biomeIndex].MaxLevel;

    public BigInteger GetCost(byte biomeIndex, ushort level) => _upgradeCurves[biomeIndex].GetCost(level);

    public float GetValue(byte biomeIndex, ushort level) => _upgradeCurves[biomeIndex].GetValue(level);
}