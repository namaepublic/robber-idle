﻿using MightyAttributes;
using TMPro;
using UnityEngine;

public class FloatingLabelBehaviour : MonoBehaviour
{
    [SerializeField, GetComponent, ReadOnly]
    private Transform _transform;

    [SerializeField, GetComponentInChildren, ReadOnly]
    private CameraFacingBillboardBehaviour _billboardBehaviour;

    [SerializeField] private Vector3 _positionOffset;
    [SerializeField] private TextMeshProUGUI _text;

    public void UpdateBehaviour() => _billboardBehaviour.SetRotation();

    public void Display(Vector3 position, string text)
    {
        gameObject.SetActive(true);
        
        _transform.rotation = Quaternion.identity;
        _transform.position = position + _positionOffset;
        _text.text = text;
        
        _billboardBehaviour.SetRotation();
    }

    public void Hide() => gameObject.SetActive(false);
}
