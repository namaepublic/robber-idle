﻿using MightyAttributes;
using UnityEngine;

public class CameraFacingBillboardBehaviour : MonoBehaviour
{
    [SerializeField, GetComponent, ReadOnly] private Transform _transform;

    public void SetRotation() => _transform.rotation = InstanceManager.GameManager.CameraRotation;
}
