﻿using System.Collections;
using System.Numerics;
using MightyAttributes;
using UnityEngine;
using UnityEngine.Serialization;

public enum CollectibleState : byte
{
    Available,
    Targeted,
    Collecting,
    Collected,
    Sold
}

public class CollectibleBehaviour : BaseTargetBehaviour
{
    [SerializeField, GetComponentsInChildren, ReadOnly]
    private MeshRenderer[] _meshRenderers;

    [Title("Animation")] [SerializeField, GetComponent, ReadOnly]
    private Animator _animator;

    [SerializeField, AnimatorParameter("Collecting"), ReadOnly]
    private int _collectingID;

    [SerializeField, AnimatorParameter("Collected"), ReadOnly]
    private int _collectedID;

    [SerializeField, GetComponentInChildren, ReadOnly]
    private FloatingLabelBehaviour _floatingLabel;

    [Title("Stats")] [SerializeField] private float _collectTime;
    [SerializeField] private ulong _basePrice;
    [SerializeField] private ushort _weight;

    private WaitForSeconds m_waitForCollect;

    [ShowProperty] public RobberBehaviour Robber { get; private set; }
    [ShowProperty] public CollectibleState State { get; private set; }

    public ulong BasePrice => _basePrice;
    public ushort Weight => _weight;

    public override void Init()
    {
        base.Init();
        if (m_waitForCollect == null) m_waitForCollect = new WaitForSeconds(_collectTime);

        SetAvailable();

        foreach (var meshRenderer in _meshRenderers) 
            meshRenderer.enabled = true;

        _animator.Rebind();
    }

    public void UpdateBehaviour()
    {
        if (State == CollectibleState.Sold) _floatingLabel.UpdateBehaviour();
    }

    public void Hide()
    {
        foreach (var meshRenderer in _meshRenderers) 
            meshRenderer.enabled = false;
        
        Robber.EndHarvest(this);
    }

    public void SetAvailable()
    {
        State = CollectibleState.Available;
        Robber = null;
    }

    public void OnCollectibleTargeted(RobberBehaviour robber)
    {
        State = CollectibleState.Targeted;
        Robber = robber;
    }

    public override void OnTargetReached(RobberBehaviour robber)
    {
        State = CollectibleState.Collecting;
        _animator.SetTrigger(_collectingID);

        StartCoroutine(Collect());
    }

    private IEnumerator Collect()
    {
        yield return m_waitForCollect;

        State = CollectibleState.Collected;
        _animator.SetTrigger(_collectedID);
    }

    public void OnCollectibleSold(BigInteger price)
    {
        State = CollectibleState.Sold;
        _floatingLabel.Display(InstanceManager.LevelManager.SellingArea.SafePosition, price.DisplayAsPowerOf10());
    }
}