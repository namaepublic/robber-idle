﻿using MightyAttributes;
using UnityEngine;

public class SellingAreaBehaviour : BaseTargetBehaviour
{
    [SerializeField] private Transform _safeTransform;
    [SerializeField] private Animator _safeAnimator;

    [SerializeField, AnimatorParameter("Bounce"), ReadOnly]
    private int _bounceID;
    
    public Vector3 SafePosition { get; private set; }

    public override void Init()
    {
        base.Init();
        SafePosition = _safeTransform.position;
    }

    public override void OnTargetReached(RobberBehaviour robber)
    {
        robber.LookAtPosition(SafePosition);
        _safeAnimator.SetTrigger(_bounceID);
    }
}
