﻿using UnityEngine;

public abstract class BaseTargetBehaviour : MonoBehaviour
{
    public Vector3 Position { get; private set; }
    
    public virtual void Init() => Position = transform.position;

    public abstract void OnTargetReached(RobberBehaviour robber);
}