﻿using System;
using System.Collections;
using System.Collections.Generic;
using MightyAttributes;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class RobberBehaviour : MonoBehaviour
{
    private enum RobberState : byte
    {
        Pending,
        Moving,
        Harvesting,
        Selling,
    }

    [SerializeField, GetComponent, ReadOnly]
    private NavMeshAgent _agent;

    [SerializeField] private float _rotationSpeed;

    [Title("GUI")] [SerializeField] private Image _weightImage;

    [SerializeField, GetComponentInChildren]
    private CameraFacingBillboardBehaviour _billboardBehaviour;

    [Title("Debug")] [ShowNonSerialized] private RobberState m_state;
    [ShowNonSerialized] private BaseTargetBehaviour m_target;
    [ShowNonSerialized] private ushort m_strength;
    [ShowNonSerialized] private ushort m_carriedWeight;

    private readonly List<CollectibleBehaviour> m_collectibles = new List<CollectibleBehaviour>();
    private Comparison<BaseTargetBehaviour> m_compareDistanceFromTargets;

    private Transform m_transform;
    private Vector3 m_directionToLook;

    public Vector3 Position => m_transform.position;

    public void SetStrength(ushort strength)
    {
        m_strength = strength;
        ComputeCarriedWeight();
        if (m_state == RobberState.Moving) SelectNextTarget();
    }

    public void Init()
    {
        m_compareDistanceFromTargets = CompareDistanceFromTargets;

        m_transform = transform;

        m_collectibles.Clear();
        m_state = RobberState.Pending;

        _billboardBehaviour.SetRotation();
    }

    public void UpdateBehaviour()
    {
        switch (m_state)
        {
            case RobberState.Moving:
            {
                _billboardBehaviour.SetRotation();

                if (_agent.remainingDistance > .01f) return;

                switch (m_target)
                {
                    case CollectibleBehaviour collectible:
                        if (collectible.State == CollectibleState.Targeted)
                            BeginHarvest(collectible);
                        else
                            SelectNextTarget();
                        break;
                    case SellingAreaBehaviour sellingArea:
                        StartCoroutine(Sell(sellingArea));
                        break;
                }

                break;
            }
            case RobberState.Selling:
            {
                m_transform.rotation = Quaternion.RotateTowards(m_transform.rotation,
                    Quaternion.LookRotation(m_directionToLook), _rotationSpeed * Time.deltaTime);
                _billboardBehaviour.SetRotation();
                break;
            }
        }
    }

    public void ForcePosition(Vector3 position) => _agent.Warp(position);

    public void LookAtPosition(Vector3 position)
    {
        _agent.updateRotation = false;
        m_directionToLook = (position - m_transform.position).normalized;
    }

    public bool IsFull() => m_strength <= m_carriedWeight;

    public bool CanCarryCollectible(CollectibleBehaviour collectible) => m_carriedWeight + collectible.Weight <= m_strength;

    private void ComputeCarriedWeight()
    {
        m_carriedWeight = 0;
        for (var i = 0; i < m_collectibles.Count; i++)
            m_carriedWeight += m_collectibles[i].Weight;

        _weightImage.fillAmount = (float) m_carriedWeight / m_strength;
    }

    public float GetDistanceFromTarget() => GetDistanceFromTarget(m_target);

    public float GetDistanceFromTarget(BaseTargetBehaviour target) => Vector3.SqrMagnitude(target.Position - Position);

    public void SelectNextTarget()
    {
        var levelManager = InstanceManager.LevelManager;
        _agent.updateRotation = true;

        SetTarget(IsFull() ? levelManager.SellingArea : GetClosestTarget());
        if (m_carriedWeight == 0 && m_target is SellingAreaBehaviour && !levelManager.HasAvailableCollectibles())
            m_state = RobberState.Pending;
    }

    public void SetTarget(BaseTargetBehaviour target)
    {
        if (m_target is CollectibleBehaviour collectible && collectible.State == CollectibleState.Targeted)
            collectible.SetAvailable();

        m_state = RobberState.Moving;

        m_target = target;
        _agent.SetDestination(target.Position);
    }

    private BaseTargetBehaviour GetClosestTarget()
    {
        var collectibles = InstanceManager.LevelManager.Collectibles;
        Array.Sort(collectibles, m_compareDistanceFromTargets);

        foreach (var collectible in collectibles)
        {
            if (!CanCarryCollectible(collectible)) continue;

            switch (collectible.State)
            {
                case CollectibleState.Available:
                    collectible.OnCollectibleTargeted(this);
                    return collectible;
                // case CollectibleState.Targeted:
                //     var previousRobber = collectible.Robber;
                //     if (previousRobber.GetDistanceFromTarget() <= GetDistanceFromTarget(collectible)) continue;
                //
                //     collectible.OnCollectibleTargeted(this);
                //     previousRobber.SelectNextTarget();
                //
                //     return collectible;
            }
        }

        return InstanceManager.LevelManager.SellingArea;
    }

    private void BeginHarvest(CollectibleBehaviour collectible)
    {
        m_state = RobberState.Harvesting;

        collectible.OnTargetReached(this);
    }

    public void EndHarvest(CollectibleBehaviour collectible)
    {
        m_collectibles.Add(collectible);
        ComputeCarriedWeight();

        SelectNextTarget();
    }

    private IEnumerator Sell(SellingAreaBehaviour sellingArea)
    {
        m_state = RobberState.Selling;

        var count = m_collectibles.Count;

        if (count > 0)
        {
            sellingArea.OnTargetReached(this);

            for (var i = 0; i < count; i++)
            {
                InstanceManager.SellingManager.Sell(m_collectibles[i]);
                yield return TimeManager.SellTime;
            }

            m_collectibles.Clear();
            ComputeCarriedWeight();
        }

        if (InstanceManager.LevelManager.IsCompleted())
            InstanceManager.BiomeManager.NextLevel();
        else
            SelectNextTarget();
    }

    private int CompareDistanceFromTargets(BaseTargetBehaviour first, BaseTargetBehaviour second) =>
        (int) (GetDistanceFromTarget(first) - GetDistanceFromTarget(second));
}