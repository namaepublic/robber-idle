﻿using UnityEngine;

public class SavedDataActions : MonoBehaviour
{
    public void ResetPlayerData() => SavedDataServices.ResetPlayerData();
}
